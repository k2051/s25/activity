db.fruits.insertMany([
	{
		"name": "Banana",
		"supplier": "Farmer Fruits Inc.",
		"stocks": 30,
		"price": 20,
		"onSale": true
	},
	{
		"name": "Mango",
		"supplier": "Mango Magic Inc.",
		"stocks": 50,
		"price": 70,
		"onSale": true
	},
	{
		"name": "Dragon Fruit",
		"supplier": "Farmer Fruits Inc.",
		"stocks": 10,
		"price": 60,
		"onSale": true
	},
	{
		"name": "Grapes",
		"supplier": "Fruity Co.",
		"stocks": 30,
		"price": 100,
		"onSale": true
	},
	{
		"name": "Apple",
		"supplier": "Apple Valley",
		"stocks": 0,
		"price": 20,
		"onSale": false
	},
	{
		"name": "Papaya",
		"supplier": "Fruity Co.",
		"stocks": 15,
		"price": 60,
		"onSale": true
	}
])

// using $count operator for total number of fruits on sale
db.fruits.aggregate([
	{
		$match:{"onSale":true}
	},
	{
		$count: "fruitsOnSale"
	}
])

// using $count operator for total number of fruits with stock>=20
db.fruits.aggregate([
	{
		$match:{
			"stocks":{
				$gt:20
			}
		}
	},
	{
		$count:"enoughStock"
	}
])

// using $avg operator to get average price of fruits onSale per supplier

db.fruits.aggregate([
	{
		$match:{"onSale":true}
	},
	{
		$group:{_id:"$supplier",avg_price:{$avg:"$price"}}
	}
])

// using $max operator to get maximum price of fruit per supplier
db.fruits.aggregate([
	{
		$group:{_id:"$supplier",max_price:{$max:"$price"}}
	}
])

// using $min operator to get minimum price of fruit per supplier
db.fruits.aggregate([
	{
		$group:{_id:"$supplier",min_price:{$min:"$price"}}
	}
])

